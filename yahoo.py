import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from yahoo_finance_api2 import share
from yahoo_finance_api2.exceptions import YahooFinanceError

PERIOD_TYPE_YEAR = share.PERIOD_TYPE_YEAR
FREQUENCY_TYPE_HOUR = share.FREQUENCY_TYPE_HOUR

class FinanceData:
    def __init__(self,company_code):
        self.my_share = share.Share(company_code)
        self.day = []
        self.night = []
        self.volume = []
    def get_data(self,period_unit,period,frequency_unit,frequency):
        
        # Try to get data
        sd_ = None
        try:
            sd_ = self.my_share.get_historical(share.PERIOD_TYPE_YEAR,1,share.FREQUENCY_TYPE_HOUR,1)
        except YahooFinanceError as e:
            print('[FAILED] ',e.message)
            return False
        
        # Separe data
        sd = {'timestamp':[],'high':[],'low':[],'volume':[]}

        for i in range(1,len(sd_['timestamp'])):
            if sd_['volume'][i] != 0 and sd_['high'][i] != None:
                sd['timestamp'].append(sd_['timestamp'][i])
                sd['high'].append(sd_['high'][i])
                sd['low'].append(sd_['low'][i])
                sd['volume'].append(sd_['volume'][i])

        p_open = 1
        for i in range(2,len(sd['timestamp'])):

            if sd['timestamp'][i] - sd['timestamp'][i-1] > 3*3600000:
                pre_close = (sd['high'][i-2]+sd['low'][i-2])/2
                close = (sd['high'][i-1]+sd['low'][i-1])/2
                open = (sd['high'][i+1]+sd['low'][i+1])/2
                self.volume.append(sd['volume'][i])
                self.night.append(open/close)
                self.day.append(pre_close/p_open)
                p_open = open

        self.volume.pop()
        self.night.pop()
        self.day.pop(0)

        print('[SUCCESS] Data was successefully obtained.')
        return True

    def load_data(self):
        x_train = np.array([[self.volume[i],self.night[i]] for i in range(int(0.8*len(self.volume)))])
        y_train = np.array([[self.day[i]] for i in range(int(0.8*len(self.volume)))])
        x_test = np.array([[self.volume[i],self.night[i]] for i in range(int(0.8*len(self.volume)),len(self.volume))])
        y_test = np.array([[self.day[i]] for i in range(int(0.8*len(self.volume)),len(self.volume))])
        return (x_train,y_train),(x_test,y_test)

    def show_volume(self):
        plt.plot(self.volume)
        plt.show()
    def show_day(self):
        plt.plot(self.day)
        plt.show()
    def show_night(self):
        plt.plot(self.night)
        plt.show()