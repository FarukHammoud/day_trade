import numpy as np
import tensorflow.keras as keras
import tensorflow as tf
import matplotlib.pyplot as plt

from yahoo import FinanceData, PERIOD_TYPE_YEAR, FREQUENCY_TYPE_HOUR

# Create model
model = keras.Sequential([
    keras.layers.Dense(64, activation=tf.nn.relu, input_shape=[2]),
    keras.layers.Dense(64, activation=tf.nn.relu),
    keras.layers.Dense(1)
  ]) 
optimizer = tf.keras.optimizers.RMSprop(0.001)

# Compile
model.compile(loss='mean_squared_error',optimizer=optimizer,metrics=['mean_absolute_error', 'mean_squared_error'])

# Data
MSFT = FinanceData('MSFT')
MSFT.get_data(PERIOD_TYPE_YEAR,1,FREQUENCY_TYPE_HOUR,1)

(x_train, y_train),(x_test,y_test) = MSFT.load_data()
np.savetxt("x_train.csv", x_train, delimiter=",")
np.savetxt("y_train.csv", y_train, delimiter=",")
np.savetxt("x_test.csv", x_test, delimiter=",")
np.savetxt("y_test.csv", y_test, delimiter=",")

x_train = keras.utils.normalize(x_train, axis=1)
x_test = keras.utils.normalize(x_test, axis=1)

# Fitting
model.fit(x_train,y_train,epochs=2000)

# Predict
val_loss, val_mav, val_mse = model.evaluate(x_test, y_test)
print(f'Validation loss: {val_loss}, Validation mav: {val_mav}')
