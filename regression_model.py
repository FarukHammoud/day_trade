import numpy as np
import tensorflow.keras as keras
import tensorflow as tf
import matplotlib.pyplot as plt

# Create model
model = keras.Sequential([
    keras.layers.Dense(64, activation=tf.nn.relu, input_shape=[2]),
    keras.layers.Dense(64, activation=tf.nn.relu),
    keras.layers.Dense(1)
  ]) 
optimizer = tf.keras.optimizers.RMSprop(0.001)

# Compile
model.compile(loss='mean_squared_error',optimizer=optimizer,metrics=['mean_absolute_error', 'mean_squared_error'])

# Data
Fahrenheit_=np.array([-140,-136,-124,-112,-105,-96,-88,-75,-63,-60,-58,-40,-20,-10,0,30,35,48,55,69,81,89,95,99,105,110,120,135,145,158,160],dtype=float)
Fahrenheit = np.array([[x,np.random.randint(10)] for x in Fahrenheit_],dtype=float)
print(Fahrenheit)
Celsius_=np.array([-95.55,-93.33,-86.66,-80,-76.11,-71.11,-66.66,-59.44,-52.77,-51.11,-50,-40,-28.88,-23.33,-17.77,-1.11,1.66,8.88,12,20,27.22,31.66,35,37.22,40.55,43.33,48.88,57.22,62.77,70,71.11],dtype=float)
Celsius = np.array([[x] for x in Celsius_],dtype=float)
# Fitting
model.fit(Fahrenheit,Celsius,epochs=2000)

# Predict
print(model.predict([[100,0]]))