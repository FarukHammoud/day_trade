import numpy as np

class Biased:
    def __init__(self,input_size):
        self.input_size = input_size
    def load_data(self,size,noise):
        x_train = np.random.rand(int(size*0.8),self.input_size)
        x_test = np.random.rand(int(size*0.2),self.input_size)

        y_train = [[np.sum(x_train,axis=1)[i]] for i in range(int(size*0.8))] + noise*(np.random.rand(int(size*0.8),1)-0.5)
        y_test = [[np.sum(x_test,axis=1)[i]] for i in range(int(size*0.2))] + noise*(np.random.rand(int(size*0.2),1)-0.5)
        return (x_train,np.array(y_train)),(x_test,np.array(np.array(y_test)))
